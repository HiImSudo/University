# gdb

* Examine a region of memory

````gdb
x/50b $rbp
````

* Jump to a line

````gdb
jump $rbp
````

* Set a breakpoint

````gdb
break function
````

* Change layout

````gdb
layout registers
````

* Change syntax

````gdb
set disassembly-flavor intel
````

* Set variables

````gdb
set $a = $rbp
````

* Dump memory

````gdb
dump ihex memory 0x500 0x600
````

* Print program counter

````gdb
print $pc
````
