# COS330 - Buffy

## Notes

* Payload generation using metasploit

````sh
msfvenom --format <perl/python/bash/...> --payload <payload_name> -n <node_sled_length>
````

* system() drops priviliges and thus cannot be used to spawn a root shell. Use execve instead. 

* You can use the following command to extract shellcode from a binary ( zsh particular ).

````sh
for i in `objdump -d prog | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{2}$' ` ; do echo -n "\\\x$i" ; done
````

* [Using gdb](./gdb.md)

* [Buffer Overflow Notes](./overflow.md)

## Summary

![img](https://image.slidesharecdn.com/bufferoverflowattacks-110222222248-phpapp02/95/buffer-overflow-attacks-25-728.jpg?cb=1298413654)

Overflow a stack variable with the following structure;
(nop, shellcode, return_inside_nope); 

What in essence happens is the CPU jumps to some location inside the No-Operation sled, and then continues to fall
through until it reaches and executes our shellcode.

It is important to note that gdb/radare2 maps the program into memory in different ways. Thus even if an application works inside gdb - it might not work outside of it.

## Resources

[Buffer Overflow Exploit](https://dhavalkapil.com/blogs/Buffer-Overflow-Exploit/)

[Stack Smashing For Fun And Profit](http://www-inst.eecs.berkeley.edu/~cs161/fa08/papers/stack_smashing.pdf)

[Stack Smashing For Fun And Profit Today](https://travisf.net/smashing-the-stack-today)

[Shellcode](http://www.vividmachines.com/shellcode/shellcode.html)

[Computerphile: Buffer Overflow Attack](https://www.youtube.com/watch?v=1S0aBV-Waeo&t=69s)

[Execve Shellcode](http://shell-storm.org/shellcode/files/shellcode-806.php)