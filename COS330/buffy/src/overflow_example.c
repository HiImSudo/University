#include <stdio.h>
#include <string.h>

void _print_raw(unsigned char *data, short breakOn, size_t size){
    int j = 0;
    for(int i = 0; i < size; i++){
        printf("0x%x ", data[i]);
        if(j++ == breakOn){
            j = 0;
            puts("");
        }
    }
}

int main(int argc, char **argv){
    char buff[256];
    
    asm("_before_overflow:");
    strcpy(buff, argv[1]);
    asm("_after_overflow:");
    return 0;
}