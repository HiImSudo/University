#include <string.h>

int main(int argc, char **argv){
    volatile char buff[24];

    asm("_bo:");
    strcpy(buff, argv[1]);
    asm("_ao:");
    return 0;
}