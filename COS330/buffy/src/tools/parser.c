/*
    General idea structure:
    2. Function to parse file
    3. Function to print output
*/

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

void printColumnNames(char* tok) {
    printf("There are the column names of the CSV file: %s\n", tok);
}

void printCell(char* output, int row, int col) {
    printf("This is the value of the cell at row %d and column %d: %s\n", row, col, output);
}

void parseCSV(int row, int col, char* buf) {
    char finalCell[256];
    char *tokenRow;
    char* tokenCol;

    if (row < 0) {
        printf("The row (%d) is out of bounds\n", row);
        return; 
    }

    if (col < 0) {
        printf("The column (%d) is out of bounds\n", col);
        return; 
    }

    tokenRow = strtok(buf, "\n");
    printColumnNames(tokenRow);
    
    for (int i = 1; i < row; i++) {
        tokenRow = strtok(NULL, "\n");
        if (tokenRow == NULL) {
            printf("The row (%d) is out of bounds\n", row);
            return;
        }
    }

    tokenCol = strtok(tokenRow, ",");

    for (int j = 1; j < col; j++) {
        tokenCol = strtok(NULL, ",");
        if (tokenCol == NULL) {
            printf("The column (%d) is out of bounds\n", col);
            return;
        }
    }

    asm("_before_overflow:");
    printf("DEBUG: Copying %zu bytes into finalCell\nDEBUG: 0x%p\n", strlen(tokenCol), finalCell);
    strcpy(finalCell, tokenCol);
    asm("_after_overflow:");
    
    printCell(finalCell, row, col);
}

int main(int argc, char **argv){
    //Taking care of argument data and file data
    char *filename = argv[1];

    char *buffer = malloc(50000);

    FILE *f = fopen(filename, "rb");
    fread(buffer, 32768, 1, f);
    fclose(f);

    printf("%s", "The column headings of the CSV file are stored at row index 1.\n");

    //Read in what row should be printed
    int row;
    printf("%s", "Please enter the row number: ");
    scanf("%d", &row);

    //Read in what column should be printed
    int col;
    printf("%s", "Please enter the column number: ");
    scanf("%d", &col);

    parseCSV(row, col, buffer);

    return 0;
}
