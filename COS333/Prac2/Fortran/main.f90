! Resources: https://www.tek-tips.com/viewthread.cfm?qid=1516435

program hello
real, dimension(5):: data
real mean, stdev
character*512, dimension(5):: res

call readData(data)
mean = computeMean(data)
stdev = computeStandardDeviation(data)
res = process(data, mean, stdev)
do i = 1,5
    print *, trim(res(i))
end do

print '(1X, A, F7.3)', 'Mean: ', mean 
print '(1X, A, F7.3)', 'Standard Deviation: ', stdev 


contains    
function process(data, mean, stdev) result (returnArr)
    character*512, dimension(5):: returnArr
    real, dimension(5):: data
    real mean, stdev
    character*512:: test, pos
    character*512:: line
    do i = 1,5
        ! print '(1X, A, F7.3, A, F7.3,A, F7.3)', 'OBVS ', mean - stdev, '||', data(i), '||', mean + stdev 
        Write( pos,'(i1)') i

        if(mean + stdev > data(i)) then
            if(mean - stdev < data(i)) then
                Write( test,'(f10.2)') data(i)
                line = 'Observation ' // trim(pos) // ': ' // trim(test) // trim(' (within 1 standard deviation of mean)')
                returnArr(i) = line
            else
                Write( test,'(f10.2)') data(i)
                line = 'Observation ' // trim(pos) // ': ' // trim(test) // trim(' (more than 1 standard deviation of mean)')
                returnArr(i) = line
            endif
        else
            Write( test,'(f10.2)') data(i)
            line = 'Observation ' // trim(pos) // ': ' // trim(test) // trim(' (more than 1 standard deviation of mean)')
            returnArr(i) = line
        endif
    end do
    end function process
    
    subroutine readData(arr)
        real, dimension(5), intent(inout):: arr
        real mean
        do i = 1,5
            print *, "Enter number: "
            read(*, *) arr(i)
        end do
    end subroutine readData
    
    function computeMean(arr) result (mean)
        real, dimension(5):: arr
        real:: mean
        do i = 1,5
            mean = mean + arr(i)
        end do
        mean = mean/5
    end function computeMean
    
    function computeStandardDeviation(arr) result (stdev)
    real:: m
    real, dimension(5):: arr
    real:: stdev
    real:: d
    d = 0
    m = computeMean(arr)
    do i = 1,5
        d = d + (arr(i) - m)**2
    end do
    stdev = sqrt(d/4)
    end function computeStandardDeviation
    
end program hello