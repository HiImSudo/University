with Ada.Text_IO;
with Ada.Strings.Unbounded.Text_IO;

With Ada.Numerics.Elementary_Functions;
With Ada.Strings;
with Ada.Strings.Unbounded;

use Ada.Strings;
use Ada.Strings.Unbounded;
use Ada.Numerics.Elementary_Functions;
use Ada.Text_IO;
use Ada.Strings.Unbounded.Text_IO;


procedure main is
    type Vector is Array(0..4) of Float;
    type sVector is Array(0..4) of Ada.Strings.Unbounded.Unbounded_String;
    type My_Float is digits 2;  

    -- Takes as input an array of five Floats and initialises them from stdin --
    procedure read(arr:in out Vector) is
    begin
        for I in 0..4 loop
            Put_Line("Enter number: ");
            arr(I) := Float'Value(Get_Line);
        end loop;
    end read;
    
    -- Computes the mean of the input array --
    function computeMean(arr:in Vector) return Float is
    mean: Float;
    begin
        mean := 0.0;
        for I in 0..4 loop
            mean := mean + arr(I);
        end loop;

        return mean/5.0;
    end computeMean;

    -- Returns The Standard Deviation Of The Input Array --
    function computeStandardDeviation(arr:in Vector) return Float is
    data: Vector;
    mean: Float;
    sum: Float;
    begin
        mean := computeMean(arr);
        for I in 0..4 loop
            data(I) := (arr(I) - mean)**2; 
        end loop;

        sum := 0.0;

        for I in 0..4 loop
            sum := sum + data(I);
        end loop;

        return Sqrt(sum/4.0);
    end;

    -- Finally output --
    function process(arr:in Vector; mean:in Float; stdev:in Float) return sVector is
    d: sVector;
    e: My_Float;

    begin
        for I in 0..4 loop
            if ( mean + stdev <= arr(I) or mean - stdev >= arr(I) ) then
                d(I) := Ada.Strings.Unbounded.To_Unbounded_String("Observation " & Integer'Image (I+1) & ": " & Float'Image(arr(I)) & "( more than 1 from the mean )");
            else
                d(I) := Ada.Strings.Unbounded.To_Unbounded_String("Observation " & Integer'Image (I+1) & ": " & Float'Image(arr(I)) & "( within 1 standard deviation from the mean )");            
            end if;
        end loop;

        return d;
    end;
    std: Float;
    data: Vector;
    result: sVector;
begin
    read(data);
    std := computeStandardDeviation(data);
    result := process(data, computeMean(data), std);

    for I in 0..4 loop
        Put_Line(result(I));
    end loop;

    Put_Line("Mean: "&Float'Image(computeMean(data)));
    Put_Line("Standard Deviation: "&Float'Image(std));

end main;
