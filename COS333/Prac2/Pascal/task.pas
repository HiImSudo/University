program Hello ;
{$mode objfpc}
Uses Math, sysutils;

type
  TStringArray = array[1..5] of string;

function returnArr(var arr:array of Real; mean: Real; stdev: Real): TStringArray;
var
i: Integer;
begin
        for i := 1 to 5 do
        begin
            if ((mean + stdev <= arr[i]) or ( mean - stdev >= arr[i] )) then
            begin
                Result[i] := ('Observation :' + FloatToStr(arr[i]) + '( more than 1 from the mean )');
            end
            else
            begin
                Result[i] := ('Observation ' + ': ' + FloatToStr(arr[i]) + '( within 1 standard deviation from the mean )');            
            end;
        end;
        exit;
end;

procedure  readData(var arr:array of Real);
var
i: Integer;
begin
    for i := 1 to 5 do
    begin
       readln(arr[i]);
    end;
end;

function computeMean(var arr:array of Real): Real;
var
i: Integer;
begin
    result := 0;
    for i := 1 to 5 do
    begin
       result := result + arr[i];
    end;

    result := result/5;
end;

function computeStandardDeviation(var arr:array of Real): Real;
var
sum, mean: Real;
i: Integer;
data:array[1..5] of Real;
begin
    result := 0.0;

    mean := computeMean(arr);
        for i := 1 to 5 do
        begin
            data[i] := power((arr[i] - mean),2); 
        end;

        sum := 0.0;

        for i :=  1 to 5 do
        begin
            sum := sum + data[i];
        end;

    result := Sqrt(sum/4.0);
end;


var
    data:array[1..5] of Real;
    res: TStringArray;
    i: Integer;
begin
    readData(data);
    res := returnArr(data, computeMean(data), computeStandardDeviation(data));
    for i := 1 to 5 do
    begin
        writeln(res[i]);
    end;
end.
