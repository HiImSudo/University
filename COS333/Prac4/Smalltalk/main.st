Object subclass: Course[
    |name students|

    new: aname[
        students := Set new.
        name = aname.
    ]

    init [
        students := Set new.
    ]

    getStudents[
        ^students
    ]

    addStudent: student[
        students add: student.
    ]

    removeStudent: studentNumber[
       students remove: (students detect: [ :student | student getStudentNumber = studentNumber]).
    ]

    addMark: student_number m: mark [
        (students detect: [ :student | student getStudentNumber = student_number]) addTestScore: mark. 
    ]

    removeMark: student_number m: mark [
        (students detect: [ :student | student getStudentNumber = student_number]) addTestScore: mark. 

    ]

    getCurrentAverageMark: student_number [
        ^(students detect: [ :student | student getStudentNumber = student_number]) getCurrentAverageMark. 
    ]

    setName: student_number name: n [
        (students detect: [ :student | student getStudentNumber = student_number]) setName: n. 
    ]

    getName: student_number [
        ^(students detect: [ :student | student getStudentNumber = student_number]) getName.
    ]
]

Object subclass: Student[
    |name student_number marks ntests tmp sum|

    new [
        " To signal that this is an abstract class. "
        ^self subclassResponsability
    ]

    init: aname[
        marks := SortedCollection new.
        ntests := 0.
        name := aname.
    ]

    setName: aname[
        name := aname.
    ]

    getName [
        ^name
    ]

    setStudentNumber: n[
        student_number := n.
    ]

    getStudentNumber [
        ^student_number
    ]

    addTestScore: x[
        marks add: x.
        ntests := ntests + 1.
    ]

    removeTestScore: x[
        marks remove: x.
        ntests := ntests - 1.
    ]

    getCurrentAverageMark [
        ^self subclassResponsability
    ]

    getMarks [
        ^marks
    ]
]

Student subclass: PartTimeStudent[
   getCurrentAverageMark [
        tmp := SortedCollection new.
        sum := 0.0.
       " Ignore two best marks "
        1 to: marks size do: [:x | tmp add: (marks at: x)].

       tmp remove: tmp last.
       tmp remove: tmp last.

       1 to: tmp size do: [:x | sum := sum + (tmp at: x) ].
       ^sum/tmp size
    ]
]

Student subclass: FullTimeStudent[
   getCurrentAverageMark [
        tmp := SortedCollection new.
        sum := 0.0.
       " Ignore two best marks "
       1 to: marks size do: [:x | tmp add: (marks at: x)].

       tmp remove: tmp first.
       tmp remove: tmp first.

       1 to: tmp size do: [:x | sum := sum + (tmp at: x) ].
       ^sum/tmp size
    ]
]

cos330 := Course new 'COS330'.
cos330 init.

mimels := PartTimeStudent new.
mimels init: 'Mimels'.
mimels setStudentNumber: 'dee'.

john := FullTimeStudent new.
john init: 'John'.
john setStudentNumber: 'abc'.

john addTestScore: 10.
john addTestScore: 15.
john addTestScore: 20.

cos330 addStudent: john.
cos330 addStudent: mimels.

john getCurrentAverageMark printNl.
cos330 addMark: 'abc' m: 25.
john getCurrentAverageMark printNl.
(cos330 getCurrentAverageMark: 'abc') printNl.

cos330 getStudents printNl.
cos330 removeStudent: (john getStudentNumber).
cos330 getStudents printNl.

(cos330 getName: 'dee') printNl.
