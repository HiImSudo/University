class Student
        def initialize()
            @marks = Array.new
            @ntests = 0
        end
        # ---- Shorthands Start ----
        def name
            @name
        end

        def name=(n)
            @name = n
        end
        
        def student_number
            @student_number
        end

        def student_number=(sn)
            @student_number = sn
        end
        
        def ntests
            @ntests
        end

        def ntests=(n)
            @ntests = n
        end
        
        def marks
            @marks
        end

        def marks=(m)
            @marks = m
        end
        # ---- Shorthands End ----
        class << self
            attr_accessor :name, :student_number, :ntests, :marks
        end

        def addTestScore(score)
            marks.push(score)
            @ntests = @ntests + 1
        end

        def +(score)
            addTestScore(score)
        end

        def removeTestScore(score)
            marks.delete(score)
            @ntests = @ntests - 1
        end

        def getCurrentAverageMark()
            raise "Subclass must override getCurrentAverageMark()"
        end
    end

class Course
        def initialize(name)
            @students = []
            @name = name
        end

        def addStudent(student)
            @students.push(student)
        end

        def removeStudent(student)
            @students.remove(student)
        end

        def addMark(snumber, mark)
            for s in @students do
                if s.student_number == snumber then
                        s+mark
                    end
                end
        end

        def removeMark(snumber, mark)
            for s in @students do
                if s.student_number == snumber then
                        s.removeMark(mark);
                    end
                end
        end

        def getCurrentAverageMark(snumber)
            @students.detect {|s| s.student_number == snumber}.getCurrentAverageMark
        end

        def setName(snumber,name)
            @students.detect {|s| s.student_number == snumber}.setName(name)
        end

        def getName(snumber)
            @students.detect {|s| s.student_number == snumber}.name
        end
    end

class PartTimeStudent < Student
    def initialize(name)
        super()
        @name = name
        @c = Array.new
        end

    def getCurrentAverageMark()
        # Ignore two best marks.
        @c = @marks.dup

        for x in [1, 2] do
                @c.delete(@c.max)           
            end
        
        @c.sum()/@c.length
    end

end

class FullTimeStudent < Student
    def initialize(name)
        super()
        @name = name
        end

    def getCurrentAverageMark()
        # Ignore two worst marks.
        @c = @marks[0..@marks.length]
        for x in [1, 2] do
                @c.delete(@c.min)           
            end
        
        @c.sum()/@c.length
    end
end
# Main

John = FullTimeStudent.new("John")
John.student_number = 1
John+80
John+20
John+30

Bud = FullTimeStudent.new("Bud")
Bud.student_number = 2
def Bud.getCurrentAverageMark
    rand(55..66)
end

COS330 = Course.new("COS330")
COS330.addStudent(Bud)
COS330.addStudent(John)
puts COS330.getCurrentAverageMark(John.student_number)
puts COS330.getCurrentAverageMark(Bud.student_number)

puts COS330.getName(1)