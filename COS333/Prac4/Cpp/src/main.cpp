#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Student{
    private:
        string name;
        string student_number;
        size_t num_tests;
        vector<float> marks;

    public:
        Student(string name, string num){
            this->name = name;
            this->student_number = num;
            num_tests = 0;
        }

        string getName(){
            return this->name;
        }

        void setName(string name){
            this->name = name;
        }

        string getStudentNumber(){
            return this->name;
        }

        void setStudentNumber(string num){
            this->student_number = num;
        }

        vector<float> getMarks(){
            vector<float> *clone = new vector<float>();
            for(float x: this->marks){
                clone->push_back(x);
            }
        
            return *clone;
        }
        
        void operator+(double a) {
            Student::addTestScore(a);
        }

        void addTestScore(float score){
            marks.push_back(score);
            num_tests++;
        }

        void removeTestScore(float score){
            int pos = -1;
            for(int i = 0; i < marks.size(); i++)
                if(marks[i] == score)
                    pos = i;

            if(pos != -1){
                marks.erase(marks.begin() + pos);    
                num_tests--;
            }
        }

        virtual float getCurrentAverageMark() = 0;
};

class PartTimeStudent: public Student{
    public:
        PartTimeStudent(string name, string num) : Student(name, num){}

        float getCurrentAverageMark(){
            float sum = 0;
            vector<float> c = getMarks();

            for(int i = 0; i < 2; i++)
                c.erase(max_element(c.begin(), c.end()));

            for(float x: c)
                sum += x;

            return sum/c.size();
        }
};

class FullTimeStudent: public Student{
    public:
        FullTimeStudent(string name, string num) : Student(name, num){}

        float getCurrentAverageMark(){
            float sum = 0;
            vector<float> c = getMarks();

            for(int i = 0; i < 2; i++)
                c.erase(min_element(c.begin(), c.end()));

            for(float x: c)
                sum += x;

            return sum/c.size();
        }
};

class Course{
    private:
        vector<Student *> students;
        string name;

    public:
        Course(string name){
            this->name = name;
        }

        void addStudent(Student *student){
            this->students.push_back(student);
        }

        void removeStudent(string studentnumber){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    students.erase(find(students.begin(), students.end(), student));
                    return;
                }
            }
        }

        void addMark(string studentnumber, float mark){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    student->addTestScore(mark);
                    return;
                }
            }
        }

        void removeMark(string studentnumber, float mark){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    student->removeTestScore(mark);
                    return;
                }
            }
        }

        float getCurrentAverageMark(string studentnumber){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    return student->getCurrentAverageMark();
                }
            }

            return 0.0; // No value found
        }

        void setName(string studentnumber, string name){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    student->setName(name);
                }
            }
        }

        string getName(string studentnumber){
            for(Student *student: this->students){
                if(student->getStudentNumber() == studentnumber){
                    return student->getName();
                }
            }

            return "stub";
        }

        vector<string> getStudents(){
            vector<string> s;
            for(Student *student: this->students){
                s.push_back(student->getName());
            }
            
            return s;
        }
};

int main(){
    Course cos330("COS330");
    
    FullTimeStudent jane("jane", "jane");
    FullTimeStudent john("john", "john");

    john.addTestScore(10);
    john.addTestScore(15);
    john.addTestScore(20);

    cos330.addStudent(&john);
    cos330.addStudent(&jane);

    cout << john.getCurrentAverageMark() << endl;
    cos330.addMark("john", 25);
    cout << john.getCurrentAverageMark() << endl;
    cout << cos330.getCurrentAverageMark("john") << endl;

    cout << cos330.getStudents().size() << endl;
    cos330.removeStudent("john");
    cout << cos330.getStudents().size() << endl;

    cout << cos330.getName("jane") << endl;
    return 0;
}