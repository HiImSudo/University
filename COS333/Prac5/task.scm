; Trial division; Divide from 2 => sqrt(n)
(define restrict(lambda (x)
    (sqrt (ceiling x))
))

(define _isPrime(lambda (n c)
    (cond
        ((= n 1) #f)
        ((= 2 n) #t)
        ((= 0 (modulo n c)) #f)
        ((= c (ceiling (sqrt n))) #t)
        (else (_isPrime n (+ c 1)))
        )
))



(define isPrime (lambda (num) 
    (_isPrime num 2)
    ))


(define _findPrimes(lambda (nums res)
    (cond ((null? nums) (list res))
          ((eq? (isPrime (car nums)) #t) (_findPrimes (cdr nums) (append res (list (car nums)))))
          ((eq? (isPrime (car nums)) #f) (_findPrimes (cdr nums) res))
        )
))


(define findPrimes (lambda (nums)
    (car (_findPrimes nums '()))
))