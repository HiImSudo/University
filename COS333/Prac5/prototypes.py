#!/bin/python3
import math

def isPrime(n):
    return _isPrime(n, 2)

def _isPrime(n, c):
    if n == 1:            # Base case; One is not a prime
        return False      
    elif n == 2:          # Base case; Two is the only even prime
        return True
    elif n % c == 0:
        return False   # If x is divisable by any number 2 => sqrt(x); This is not a prime.
    elif c == math.ceil(math.sqrt(n)):
        return True     # If we reach this statement before False/True is returned, the number is a prime.
    else:
        return _isPrime(n, c + 1)


def findPrimes(numbers):
    res = []
    for x in numbers:
        if isPrime(x):
            res.append(x)
    return res


print(findPrimes([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]))

