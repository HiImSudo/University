# Scheme

This is just my exploration of the scheme language.
[This](https://www.youtube.com/watch?v=byofGyW2L10) is also a very good resource.

## Variables

````scheme
(define name "Connor")
````

## Functions

````scheme
(define say_hello (lambda()
    "Hello world"))
````

The below example sums a b and c.

````scheme
(define sum(lambda(a b c)
    (+ a b c)
))
````

You can also use the shorter form

````scheme
(define (sum a b c)
    (+ a b c))
````