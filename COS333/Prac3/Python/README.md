# Python

Please note that my practical explicitly requires the use of python3 as I made use of typehints. It can be installed by running

````sh
sudo pacman -S python3
sudo apt-get install python3
sudo dnf install python3
````

When the program is start the user can input a new string by selecting number 2 and trying to input the string. After the string has been
processed you can pickle it to a file in the current directory.

After the file has been created you can rerun the program and try to read the pickle by using menu number 1.