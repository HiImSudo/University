#!/bin/python3

import string;
import pickle;

# The convertInput function ensures 
def convertInput(buffer: str) -> str:
    result: str = "";
    for c in buffer:
        if c.isalpha() == True:
            result = result + c.lower();
    return result;

# The reverse function reverses a string
def reverse(buffer: str):
    return buffer[::-1];

def searchString():
    buffer: str = convertInput(input("Enter the raw string: "));
    word: str = input("Enter the word to search for: ");

    print("Forward: " + str(buffer.count(word)));
    print("Backwards: " + str(reverse(buffer).count(word)));
    pickleConvertedInput( "Forward: " + str(buffer.count(word)) + "\nBackwards: " + str(reverse(buffer).count(word)),input("Enter a filename to save the pickle as: "));


def pickleConvertedInput(buffer: str, filename: str):
    pickle.dump(buffer, open(filename, "wb"));

def getPickledConvertedInput(filename: str):
    print("- - - - - - - - - - - - - - - - - - - - - PICKLE - - - - - - - - - - - - - - - - - - - - -");
    print(pickle.load(open( filename, "rb" )));

def menu():
    print("Menu:\n\t 1) Read from pickle\n\t 2) Input new string");
    choice = input();
    if choice == "1":
        getPickledConvertedInput(input("Please enter the filename: "));
    elif choice == "2":
        searchString();
    else:
        menu();

menu();