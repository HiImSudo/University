// Cleans the string.
function clean(buffer){
    return buffer.toLowerCase().replace(/[^a-z]/gi, '');
}

// Split on every character, reverse the resulting array, join the 
// resulting array with no delim.
function reverse(buffer){
    return buffer.split("").reverse().join("");
}

/* It is very disappointing that a common way to
 * count string occurences is via split.
 */
function count(haystack, needle){
    return haystack.split(needle).length - 1;
}

function task(raw, word){
    document.getElementById('placeholder_f').innerText = "Forwards: " + count(clean(raw), word);
    document.getElementById('placeholder_b').innerText = "Backwards: " + count(reverse(clean(raw)), word)
}

var has_run = false;

function task_clean(){
    var needle = document.getElementById("needle");
    has_run = true;

    if(needle == null){
        alert("Can't find the needle.");
    }else{
        haystack.value = clean(haystack.value);
    }   
}

function task_wrapper(){
    if(has_run){
        var haystack = document.getElementById("haystack");
        var needle = document.getElementById("needle");

        if(haystack == null || needle == null){
            alert("Can't find the haystack || needle.");
        }else{
            task(haystack.value, needle.value);
            haystack.value = clean(haystack.value);
        }      
    }else{
        alert("Please run clean first!");
    }
}
