<html>

<h>
Text: hellothisisatestsiisspanishforyes
</h>
<?php
// Removes non-alpha characters.
function convertInput($buffer){
    return preg_replace("/[^a-z]/", '', strtolower($buffer));
}

function searchString($haystack, $needle){
    // echo "This echo is to make things easier for the marker!\r\n";
    // echo "Forwards: ".substr_count(convertInput($haystack), $needle)."\r\n";
    // echo "Backwards: ".substr_count(convertInput(strrev($haystack)), $needle)."\r\n";

    return [substr_count(convertInput($haystack), $needle), substr_count(convertInput(strrev($haystack)), $needle)];
}

$data = searchString("hellothisisatestsiisspanishforyes", "is");
// The above array can be indexed from [0..1]

echo "<p> Forward Matches: $data[0] </p>";
echo "<p> Backwards Matches: $data[1] </p>";
?> 

</html>