# PHP

The added makefile can be used to run the script
or simply the command:

````sh
    php ./script.php
````

It will output the results twice;

The first output is to make marking easier for the marker.
The second is to showcase indexing of arrays in php.