public class Office extends Venue{
    public String Name;
    public String Surname;
    public String PhoneNumber;
    public Office(Building building, Integer roomnumber, String Name, String Surname, String PhoneNumber){
        super(building, roomnumber);
        this.Name = Name;
        this.Surname = Surname;
        this.PhoneNumber = PhoneNumber;
    }

    @Override
    public String toString(){
        return super.toString() + "User Details: " + Name + " " + Surname + " -> " + PhoneNumber;

    }
}