import com.db4o.Db4o;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import com.db4o.query.Query;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;

public class Database {
    public ObjectContainer db;

    public void open() {
        db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "database.db4o");
    }

    public void close() {
        db.close();
    }

    public void addBuilding(BufferedReader reader) throws IOException {
        System.out.println("Enter the building name: ");
        String name = reader.readLine();
        db.store(new Building(name));
    }

    public void addLectureHall(BufferedReader reader) throws IOException {
        String name = Menu.getString("Enter the lecture hall name:");
        Integer roomnumber = Menu.getInteger("Enter the lecture hall room number:");
        Integer nseats = Menu.getInteger("Enter the number of seats:");
        db.store(new LectureHall(new Building(name), roomnumber, nseats));
        
    }

    public void addOffice(BufferedReader reader) throws IOException {
        String name = Menu.getString("Enter the office name:");
        Integer roomnumber = Menu.getInteger("Enter the office room number:");

        String username = Menu.getString("Enter the office user's name:");
        String surname = Menu.getString("Enter the office user's surname:");
        String number = Menu.getString("Enter the office user's phone number:");

        db.store(new Office(new Building(name), roomnumber, username, surname, number));
    }

    public void listVenues() {
        System.out.println("These are the available venues:");

        // Alternatively db.queryByExample(Venue.class)
        // Then use instanceof() to determine the class type

        System.out.println("_____________ OFFICES ______________");

        for (Object x : db.queryByExample(Office.class)) {
            Office o = (Office) x;     // This cast should be fairly safe.
            System.out.println(o);
        }

        System.out.println("_____________ LECTURE HALLS ______________");

        for (Object x : db.queryByExample(LectureHall.class)) {
            LectureHall l = (LectureHall) x;     // This cast should be fairly safe.
            System.out.println(l);
        }
    }

    public void output(List set) {
        for (Object x : set) {
            System.out.println(x);
        }
    }

    public void findOffice(BufferedReader reader) throws IOException {
        String officeName = Menu.getString("Enter office owner's name to search for: ");
        for (Object o : db.queryByExample(new Office(new Building(officeName), null, null, null, null))) {
            Office office = (Office) o;
            System.out.println("\tThe office is at room " + office.roomnumber + " in " + office.building.Name);
            return;
        }
        System.out.println("The office could not be found!");
    }

    public void findLectureHall(BufferedReader reader) throws IOException {


        String lectName = Menu.getString("Enter Lecture Hall's name to search for: ");
        
        Query query = db.query();
        query.constrain(LectureHall.class);

        // This is a basic SODA query
        List<LectureHall> result = query.execute();
        if(result.size() > 0)
            System.out.println(result);
        else
            System.out.println("The lecture hall could not be found!");

    }

    public void updateVenue(BufferedReader reader) throws IOException {
        String name = Menu.getString("Enter the name of the building to update: ");
        for (Object o : db.queryByExample(LectureHall.class)) {
            LectureHall hall = (LectureHall) o;
            if (hall.building.Name.equals(name)) {
                Integer seats = Menu.getInteger("How many seats should the hall have: ");
                hall.NumberOfSeats = seats;
                db.store(hall);
                return;
            }
        }

        for (Object o : db.queryByExample(Office.class)) {
            Office office = (Office) o;
            if (office.building.Name.equals(name)) {
                office.Name = Menu.getString("Enter the new occupant's name: ");
                office.Surname = Menu.getString("Enter the new occupant's surname: ");
                office.PhoneNumber = Menu.getString("Enter the new occupant's phone number: ");

                db.store(office);
                return;
            }
        }
    }

    private ObjectContainer database;
    private final String fileName = "database.db";
}