public class Venue{
    public Building building;
    public Integer roomnumber;
    public Venue(Building b, Integer n){
        building = b;
        roomnumber = n;
    }

    @Override
    public String toString(){
        return building.toString() + "Room Number: " + roomnumber + "\n";
    }
}