public class LectureHall extends Venue{
    public Integer NumberOfSeats;
    public LectureHall(Building b, Integer n, Integer seats){
        super(b, n);
        NumberOfSeats = seats;
    }

    @Override
    public String toString(){
        return super.building + "Number Of Seats: " + NumberOfSeats;
    }
}