import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Menu{
    public static String getString(String message){
        System.out.println(message);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String buffer;
        try{
            buffer = reader.readLine();
        }catch (IOException ex){
            buffer = "nill";
        }
        return buffer;
    }

    public static Integer getInteger(String message){
        System.out.println(message);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer buffer;
        try{
            buffer = Integer.parseInt(reader.readLine());
        }catch (IOException ex){
            buffer = 0;
        }
        return buffer;
    }
}