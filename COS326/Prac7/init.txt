// - - - - - - - - - - - - - - - QUESTION 1 - - - - - - - - - - - - - - - 

// Delete previous
MATCH (n) DETACH DELETE n

// Init
CREATE (Thandi:Person {name:'Thandi', age:20, from:'Durban'}),
(Johan:Person {name:'Johan', age:19, from:'Pretoria'}),
(Neo:Person {name:'Neo', age:22, from:'Tshwane'}),
(Melanie:Person {name:'Melanie', age:21, from:'Johannesburg'}),
(Trees:Tweet {hashtag:'#Trees', message:"provide oxygen"}),
(Rhinos:Tweet {hashtag:'#Rhinos', message:"are innocent"}),
(Thandi)-[:FOLLOWS{since: 2014}]->(Johan),
(Thandi)-[:FOLLOWS{since: 2011}]->(Neo),
(Johan)-[:FOLLOWS{since: 2012}]->(Thandi),
(Neo)-[:FOLLOWS{since: 2016}]->(Thandi),
(Neo)-[:FOLLOWS{since: 2016}]->(Melanie),
(Melanie)-[:FOLLOWS{since: 2012}]->(Johan),
(Melanie)-[:FOLLOWS{since: 2012}]->(Thandi),
(Thandi)-[:TWEETED{date: "20-08-2018"}]->(Trees),
(Johan)-[:RETWEETED{date: "20-08-2018"}]->(Trees),
(Neo)-[:TWEETED{date: "05-09-2018"}]->(Rhinos),
(Melanie)-[:FAVORITED{date: "05-09-2018"}]->(Rhinos),
(Trees)-[:TWEETED {since: "20-08-2018"}]->(Thandi),
(Trees)-[:RETWEETED {since: "20-08-2018"}]->(Thandi),
(Rhinos)-[:TWEETED {since: "05-09-2018"}]->(Neo),
(Rhinos)-[:FAVORITE {since: "05-09-2018"}]->(Melanie)

// - - - - - - - - - - - - - - - TASK 1 QUESTION 2 - - - - - - - - - - - - - - - 
// b i)
MATCH (n) RETURN distinct labels(n)

// b ii)
MATCH (person) 
WHERE person:Person
RETURN person.name
ORDER BY person.name

// b iii)
MATCH (t) WHERE t:Tweet RETURN t.hashtag ORDER BY t.hashtag

// b iv)
MATCH (n)-[rel]->(b) 
RETURN distinct type(rel)

// b v)
MATCH(p:Person)-[:TWEETED]->(t:Tweet) RETURN p.name

// b vi)
MATCH (n)-[:RETWEETED]->(m)
WHERE NOT n.hashtag = "null"
RETURN n.hashtag, n.message

// b vii)
MATCH(p:Person)-[:FAVORITED]->(t:Tweet) RETURN t.hashtag, t.message



