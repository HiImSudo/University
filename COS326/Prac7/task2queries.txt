// - - - - - - - - - - - - - - - TASK 2 QUESTION  1 - - - - - - - - - - - - - - - 

// a i
MATCH (psN:Person {name:"Neo"})-[*1..2]-(t)
WHERE t:Tweet
RETURN DISTINCT t

// a ii
MATCH p1 = (n1:Person{name: "Melanie"})-[r1*..5]->(n2:Person{name: "Neo"}), p2 = (n2:Person)-[r2*..5]->(n3:Person) 
WHERE all(x1 in nodes(p1) WHERE (x1:Person) OR (x1:Tweet)) 
AND all(x2 in nodes(p2) WHERE (x2:Person) OR (x2:Tweet)) 
RETURN p1

// a iii
MATCH (p:Person)-[:TWEETED]->(t)
RETURN p.name, EXISTS(()-[:RETWEETED]->(p))

// a iv
MATCH path=shortestPath((pe:Person {name:'Melanie'})-[*0..10]->(pl:Person {name:'Neo'}))
RETURN path


// b i
MATCH(p:Person), (t:Tweet)
RETURN count( DISTINCT p) + count( DISTINCT t)

// b ii
MATCH (p:Person)-[r:FOLLOWS]->(:Person) RETURN distinct p.name, COUNT(r);

// b iii
MATCH (p:Person)<-[r:FOLLOWS {since: 2014}]-(:Person) 
RETURN DISTINCT p.name, COUNT(r)

// 2b iv
MATCH (p:Person)<-[r:FOLLOWS]-(:Person)
RETURN DISTINCT p.name, COUNT(r) AS c
ORDER BY c DESC
LIMIT 1