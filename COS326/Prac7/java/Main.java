import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.TransactionWork;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.neo4j.driver.v1.Values.parameters;

public class Main implements AutoCloseable
{
    public final Driver driver;

    public Main( String uri, String user, String password )
    {
        driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
    }

    @Override
    public void close() throws Exception
    {
        driver.close();
    }

    public void printGreeting( final String message )
    {
        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    StatementResult result = tx.run( "CREATE (a:Greeting) " +
                                                     "SET a.message = $message " +
                                                     "RETURN a.message + ', from node ' + id(a)",
                            parameters( "message", message ) );
                    return result.single().get( 0 ).asString();
                }
            } );
            System.out.println( greeting );
        }
    }

    public static void menu(Session session){
        try{
        System.out.println("Make a selection:");
        System.out.println("\t1 - Count person nodes");
        System.out.println("\t2 - Count number of people each person follows");
        System.out.println("\t3 - Count since 2014");
        System.out.println("\t4 - Find name and age of youngest person in network");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String buffer = reader.readLine();
        Integer choice = Integer.parseInt(buffer);

        switch(choice){
            case 1:{
                System.out.println("CASE 1");

                // Auto-commit transactions are a quick and easy way to wrap a read.
                StatementResult result = session.run(
                        "MATCH (a:Person) RETURN COUNT(a.name) AS name");
                // Each Cypher execution returns a stream of records.
                while (result.hasNext()) {
                        Record record = result.next();
                        // Values can be extracted from a record by index or name.
                        
                        System.out.println("=> "+ record.get("name"));
                    }
                break;
            }

            // 
            case 2:{
                System.out.println("CASE 2");

                // Auto-commit transactions are a quick and easy way to wrap a read.
                StatementResult result = session.run(
                    "MATCH x = (person:Person)-[r:FOLLOWS]->(:Person) RETURN distinct person.name, COUNT(r);");
                // Each Cypher execution returns a stream of records.
                while (result.hasNext()) {
                        Record record = result.next();
                        // Values can be extracted from a record by index or name.
                        
                        System.out.println(record.get(0) + "=> "+ record.get(1));
                    }
                break;
            }
            case 3:{
                System.out.println("CASE 3");

                // Auto-commit transactions are a quick and easy way to wrap a read.
                StatementResult result = session.run(
                    "MATCH x = (:Person)-[r:FOLLOWS]->(person:Person) WHERE r.since > 2014 RETURN person.name, COUNT(r);");
                // Each Cypher execution returns a stream of records.
                while (result.hasNext()) {
                        Record record = result.next();
                        // Values can be extracted from a record by index or name.
                        
                        System.out.println(record.get(0) + "=> "+ record.get(1));
                    }
                break;
            }
            case 4:{
                System.out.println("CASE 4");

                // Auto-commit transactions are a quick and easy way to wrap a read.
                StatementResult result = session.run(
                    "MATCH (person:Person) RETURN MIN(person.age), MIN(person.name);");     // This is sketch; this min probably doesn't work.
                // Each Cypher execution returns a stream of records.
                while (result.hasNext()) {
                        Record record = result.next();
                        // Values can be extracted from a record by index or name.
                        
                        System.out.println(record.get(1) + "=> "+ record.get(0));
                    }
                break;
            }
        }
    }catch(Exception ex){
        
    }
    }
    public static void main( String... args ) throws Exception
    {
        try ( Main greeter = new Main( "bolt://localhost:7687", "neo4j", "Eternallove96" ) )
        {
            try (Session session = greeter.driver.session())
        {
            
            while(1 == 1){
                menu(session);
            }
        }
        }
    }
}
