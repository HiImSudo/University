/*
    This function just prints something.
*/
CREATE OR REPLACE FUNCTION say(msg text)
RETURNS text AS $$
BEGIN
    RETURN msg;
END;
$$ LANGUAGE PLpgSQL;

/*
    This function appends two strings.
*/
CREATE OR REPLACE FUNCTION concat(a text, b text)
RETURNS text AS $$
BEGIN
    RETURN a || b;
END
$$ LANGUAGE PLpgSQL;

SELECT concat('hello ', 'world');