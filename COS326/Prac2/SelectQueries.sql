DROP FUNCTION personFullNames(NAME_COMP);
CREATE OR REPLACE FUNCTION personFullNames(a NAME_COMP)
RETURNS text AS $$
BEGIN
    RETURN CONCAT(CONCAT(CONCAT(CONCAT(a.Title, ' '),a.Firstname), ' '), a.Surname);
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION ageInYears(date);
CREATE OR REPLACE FUNCTION ageInYears(birthday date)
RETURNS int AS $$
BEGIN
    RETURN extract(year from current_date) - EXTRACT(Year from birthday);
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isFinalYear(int);
CREATE OR REPLACE FUNCTION isFinalYear(b int)
RETURNS boolean AS $$
BEGIN
    RETURN b = 3; 
    /*
        Assuming all courses are 3 years long.
    */
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isFullTime(bool);
CREATE OR REPLACE FUNCTION isFullTime(b boolean)
RETURNS boolean AS $$
BEGIN
    RETURN b; 
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isPartTime(bool);
CREATE OR REPLACE FUNCTION isPartTime(b boolean)
RETURNS boolean AS $$
BEGIN
    RETURN NOT(b); 
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isRegisteredFor(text[], text);
CREATE OR REPLACE FUNCTION isRegisteredFor(modules text[], module text)
RETURNS boolean AS $$
BEGIN
    RETURN modules @> ARRAY[module];
END
$$ LANGUAGE PLpgSQL;

/*
    Select all student details.
*/
SELECT StudentNumber, personFullNames(StudentName), ageInYears(Birthday)   FROM Students;

/*
    Select all undergraduate students.
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, CurrentCourses FROM Undergraduates;

/*
    Select all postgraduate students.
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, personFullNames(Supervisor) FROM Postgraduates;


/*
    Final year undergraduates
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, CurrentCourses FROM Undergraduates
    WHERE CAST(isFinalYear(YearsOfStudy) AS boolean) = true;


/*
    Undergraduates registered for COS326

    Where registered courses has cos326
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, CurrentCourses FROM Undergraduates
    WHERE isRegisteredFor(CAST(CurrentCourses as text[]), CAST('COS326' AS text));

/*
    All full time postgrads
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, personFullNames(Supervisor) FROM Postgraduates
    WHERE CAST(isFullTime(IsFullTime) AS boolean) = true;

/*
    All part time postgrads
*/
SELECT  StudentNumber, personFullNames(StudentName), Degree, YearsOfStudy, personFullNames(Supervisor) FROM Postgraduates
    WHERE CAST(isPartTime(IsFullTime) AS boolean) = true;