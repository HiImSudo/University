# Postgresql

## Setup

````sh
sudo pacman -S postgresql pgadmin3
sudo -u postgres -i
initdb -D '/var/lib/postgres/data'
````

## Interfacing

To start a shell.

````sh
psql -U postgres -W
````

To run a sql file
````sh
psql -U postgres -f file.sql
````