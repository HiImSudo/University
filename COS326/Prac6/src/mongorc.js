/*
TASK 1:

Test the above functions in the mongo shell as follows:
1. Create a database called prac6db, a collection called Cartesian and a composite index on the xval and
yval values. [2 mark]
2. Use the function insertPoints(dbName, colName, xcount, ycount) to insert the objects
{ x: xval, y: yval} into the collection Cartesian in database prac6db, for xcount = 5, ycount = 5.
[3 marks]
3. Use the function findNearest((dbName, colName, xval, yval) to find and print the point which is
nearest to coordinates (5, 7). [5 marks]
4. Use the function updateYVals(dbName, colName, threshold,incr) to update the objects { x: xval, y:
yval} in the collection Cartesian in database prac6db. Use a incr value of 10 and a threshold value of 2.
[3 marks]
5. Use the function removeIfYless(dbName, colName, threshold) to delete (remove) objects
{ x: xval, y: yval} from the collection Cartesian in database prac6d. Every document with yval < 4
should be removed
*/
function print_all(){
  db.getSiblingDB('prac6db').getCollection('Cartesian').find().pretty();
}

function distance(x1, y1, x2, y2){
    return Math.sqrt( Math.pow((x1-x2), 2) + Math.pow((y1-y2), 2));
}

function insert(dbn, col, x, y){
    db.getSiblingDB(dbn).getCollection(col).insert({"xval": x, "yval": y});
}

function findNearest(dbn, col, x, y){
    col = db.getSiblingDB(dbn).getCollection(col);
    data = col.find().toArray();
    min = 99999;
    obj = data[0];

    data.forEach(val => {
      if (distance(x, y, val.xval, val.yval) < min){
        obj = val;
      }
    });

    print("x: " + obj.xval + " ;y: " + obj.yval);
}

function updateYVales(dbn, col, threshold, incr){
    col = db.getSiblingDB(dbn).getCollection(col);
    data = col.find().toArray();

    data.forEach(val => {
      if(val.yval > threshold){
          old = val.yval;
          val.yval += incr;
          print("Updating!");
          col.update({"xval": val.xval, "yval": old},{"xval": val.xval, "yval": val.yval});
      }
    });
}

function removeIfYLess(dbn, col, threshold){
    col = db.getSiblingDB(dbn).getCollection(col);
    data = col.find().toArray();

    data.forEach(val => {
      if(val.yval < threshold){
          col.remove(val);
      }
    });
}


/* 
  TESTS:

*/

// - - - - - - - - - - - - - - - - - - - - - - -  TASK 1 END - - -  - - - - - - 

/*
  TASK 2:

1. Use the allStatesPopulation(dbName, colName) function to show the state name and population for all
states as a list of {state, population} pairs sorted in alphabetical (ascending) order of state name.
[5 marks]
2. Use the oneStatePopulation(dbName, colName, stateName) function to show the state name and
population for one state of your choice as a{state, population} pair. [5 marks]
3. Use the allStatesPopulationMR(dbName, colName) function to compute and show the total population
of each state. [5 marks]
4. Use the placesInGrid( dbName, colName, lat1,lat2,lon1,lon2) function to show the zip code, city name,
location (loc) as [latitude, longitude] and state for the places located between latitudes lat1,lat2 and
longitudes lon1 and lon2 of your choice. [5 marks]
*/

// Import: mongoimport ./json/fb.json
// Import: mongoimport ./json/zipcodes.json
function allStatesPopulation(dbn, col){
  db.getSiblingDB(dbn).getCollection(col).aggregate([
      {$group: {_id: "$state", population: {$sum: "$pop"}}},
      { $sort : { population : 1} }
    ]).forEach(x => {printjson(x)});
}

function oneStatePopulation(dbn, col, st){
    db.getSiblingDB(dbn).getCollection(col).aggregate([
      {$match: {state: st}},
      {$group: {_id: "$state", population: {$sum: "$pop"}}} 
    ]).forEach(x => {printjson(x)});
}

function allStatesPopulationMR(dbn, col){
  db.getSiblingDB(dbn).getCollection(col).mapReduce(
      () => {
          emit(this.state, this.population);
      },
      (key, values) => {
          return Array.sum(values);
      },
      {
        query: {},
        out: "states_population"
      }
  ).forEach(x => {printjson(x)});
}

function isBetween(element, lat1, lat2, lon1, lon2){
  lat = element["loc"][0]; lon = element["loc"][1];
  if (lat1 <= lat && lat <= lat2)
    if (lon1 <= lon && lon <= lon2)
      return true;
  return false;
}

function placesInGrid(dbn, col, lat1, lat2, lon1, lon2){
  db.getSiblingDB(dbn).getCollection(col).find({}).toArray().forEach(element => {
      if(isBetween(element, lat1, lat2, lon1, lon2)){
          printjson({
            "zip_code": element._id,
            "city_name": element.city,
            "loc": element.loc,
            "state": element.state,
          });
      }
  });
}