
/*Merissa Joubert u15062440 Prac 6 Task1*/
function insertPoints(dbName, colName, xcount, ycount) {
    var mydocument = db.getSiblingDB(dbName).getCollection(colName);
    mydocument.createIndex({x: 1, y: 1});
    for(var i = 1, n = 2; i*n <= xcount && i*n <= ycount; i++)
        mydocument.insert({x: i*n, y: i*n});        
}

function findNearest(dbName, colName, xval, yval) {
    var mydocument = db.getSiblingDB(dbName).getCollection(colName);
    var otherDocument = mydocument.find();        
    var found, diff, item;
    var min = Number.MAX_VALUE;
    while(otherDocument.hasNext()) {
        item = otherDocument.next();
        diff = Math.sqrt(Math.pow((item.x - xval), 2) + Math.pow(item.y - yval, 2));
        if(diff < min) {
            min = diff;
            found = item['_id'];
        }                                  
    }

    otherDocument = mydocument.find({_id: found});
    if(otherDocument.hasNext()) {            
        printjson(otherDocument.next());            
    }
}

function updateYVals(dbName, colName, threshold, step) {
    var mydocument = db.getSiblingDB(dbName).getCollection(colName);
    var otherDocument = mydocument.find();
    var item;        
    while(otherDocument.hasNext()) { 
        item = otherDocument.next();           
        if(item.y > threshold) {
            mydocument.update({_id: item._id},{x: item.x, y: item.y + step});
        }            
    }
}

function removeIfYless(dbName, colName, threshold) {
    var mydocument = db.getSiblingDB(dbName).getCollection(colName);
    var otherDocument = mydocument.find();
    var item;    
    while(otherDocument.hasNext()) {
        item = otherDocument.next();            
        if(item.y < threshold) {
            mydocument.remove({_id: item._id});
        }        
    }
}

//task 2
function allStatesPopulation(dbName,colName)
{
	var mydocument = db.getSiblingDB(dbName).getCollection(colName);
	 
	var otherDocument = mydocument.aggregate([        
        {$project: {state: "$state", population: "$pop"}},
        {$sort: {state: 1}}
    ]);
	
	while(otherDocument.hasNext())
	{
		printjson(otherDocument.next());
	}
	 
}

function oneStatePopulation(dbName,colName,stateName)
{
	var mydocument = db.getSiblingDB(dbName).getCollection(colName);
	var otherDocument = mydocument.aggregate([
		{$match: {state: stateName}},      
        {$project: {state: "$state", population: "$pop"}}    
	]);
	
	while(otherDocument.hasNext())
	{
		printjson(otherDocument.next());
	}
}

function allStatesPopulationMR(dbName,colName)
{
	var mydocument = db.getSiblingDB(dbName).getCollection(colName);
		mydocument.mapReduce(
        function() {
            emit(this.state, this.pop);
        },
        function(key, value) {
            return {key, value};
        },
        {out: "states_population"}
    );
}
//(-70,40,-73,45)
function placesInGrid( dbName, colName, lat1,lat2,lon1,lon2) {
    var mydocument = db.getSiblingDB(dbName).getCollection(colName);
    var otherDocument= mydocument.aggregate([
		{		
			$match: 
			{
				$and : [{"loc.0": {$gt:lat1}},{"loc.0": {$lt:lat2}},{"loc.1": {$gt:lon1}},{"loc.1": {$lt:lon2}}]
			}
		},
		{		
			$project: 
			{
				"_id":0,
				"zipcode":"$_id",
				"cityname": "$city",
				"location": "$loc",
				"state":1
			}
		}]);
		
		while(otherDocument.hasNext())
		{
			printjson("Zipcode:" +otherDocument.next().zipcode+" CityName: "+otherDocument.next().cityname+" Location: "+otherDocument.next().location);
			
		}
 printjson("here");
}


