/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseconnect;
import java.util.List;
import java.util.Set;
import java.util.*;
import com.mongodb.MongoClient;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;


public class DatabaseConnect {

        static void question1(DB db) 
        {
            Set<String> colls = db.getCollectionNames();
       
        for (String collectionName : colls) 
        {
            System.out.println(collectionName);
        }
	}
	
	static void question2(DB db) {
		DBCollection col = db.getCollection("facebook");
		DBCursor cursor = col.find();
		DBObject item;
		try {
			while(cursor.hasNext()) {
				item = cursor.next();								
				System.out.println(item);				
			}
		} finally {
			cursor.close();			
		}
	}
	
	static void question3(DB db) {
		BasicDBObject query = new BasicDBObject("message", new BasicDBObject("$ne", ""));
        DBCollection fbCol = db.getCollection("facebook");
        DBCursor cursor = fbCol.find(query);
        List<DBObject> objects = cursor.toArray();
        int count = 0;
        int total = 2;
        for (DBObject dbo : objects)
        {
	        for (int i = 0; i < total; i++)
	        {
		       	 DBObject obj = ((DBObject) dbo.get("data"));
		       	 DBObject data = ((DBObject) obj.get(Integer.toString(count)));
		       	 Object data2 = (data.get("message"));
		       	 System.out.println(data2);
		       	 count++;
	        }
        }
        System.out.println("Total messages: " + total);
	}
	
	static void question4(DB db) {
		DBCollection col = db.getCollection("facebook");
		DBCollection coll = db.getCollection("messages");	
		DBCursor cursor = col.find();
		DBObject item;
		try {
			while(cursor.hasNext()) {
				item = cursor.next();												
				//Question 4
				BasicDBList data = (BasicDBList) item.get("data");
				for(Object i: data) {
					DBObject from = (DBObject) ((DBObject) i).get("from");	
					BasicDBObject doc = new BasicDBObject("from", new BasicDBObject("name", from.get("name"))
							.append("id", from.get("id"))).append("message", ((DBObject) i).get("message"));
					coll.insert(doc);					
				}	
			}
			
			System.out.println("Messages updated");
		} finally {
			cursor.close();			
		}
	}
	
	static void question5(DB db) {
		DBCollection coll = db.getCollection("messages");	
		BasicDBObject query = new BasicDBObject("from.name", "Peyton Manning");		
		DBCursor cursor = coll.find(query);
		System.out.println(cursor.count());
	}
	
	public static void main(String[] args) {
		try{
			MongoClient mongo = new MongoClient("localhost", 27017);
			DB db = mongo.getDB("prac6db");			
			System.out.println(db.getName()+" successfully connected.");
                        
			System.out.println("Question 1");
			question1(db);
			
			System.out.println("Question 2");
			question2(db);
			
			System.out.println("Question 3");
			question3(db);
			
			System.out.println("Question 4");
			question4(db);
			
			System.out.println("Question 5");
			question5(db);
		}
		catch(Exception e){
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      }
	}
}
