DROP DATABASE studentsdb;
CREATE DATABASE studentsdb;

CREATE TYPE NAME_COMP AS (
    Firstname  varchar(40),
    Surname    varchar(40),
    Title      varchar(40)
);

CREATE DOMAIN student_number AS TEXT
CHECK (
  VALUE ~ '^\d{6}$'
);

CREATE DOMAIN study_years AS TEXT
CHECK (
  VALUE ~ '^[0-9]{2}$'
);


DROP TABLE Students CASCADE;
CREATE TABLE Students (
    StudentNumber   student_number PRIMARY KEY,
    StudentName            NAME_COMP,
    Birthday   date,
    Degree    text,
    YearsOfStudy int            
);

DROP TABLE Undergraduates CASCADE;
CREATE TABLE Undergraduates (
    CurrentCourses text[]
) INHERITS (Students);

DROP TABLE Postgraduates CASCADE;
CREATE TABLE Postgraduates (
    IsFullTime boolean,
    Supervisor NAME_COMP 
)  INHERITS (Students);


DROP TABLE Degree CASCADE;
CREATE TABLE Degree (
    DegreeCode   varchar(30) PRIMARY KEY,
    DegreeName   varchar(30),    
    Years        integer,
    Department   varchar(30),
    Faculty      varchar(30)
);

DROP TABLE Courses CASCADE;
CREATE TABLE Courses (
    Code              char(30) PRIMARY KEY,
    CourseName        varchar(30),
    Department        varchar(30),
    CourseCredits     integer
);



-- NEW TABLES --

DROP TABLE DeletedUndergrad CASCADE;
CREATE TABLE DeletedUndergrad (
    CurrentCourses text[],
    UserId text,
    DateTimeOfDeletion date    
) INHERITS (Students);


DROP TABLE DeletedPostgrad CASCADE;
CREATE TABLE DeletedPostgrad (
    IsFullTime boolean,
    Supervisor NAME_COMP,
    UserId text,
    DateTimeOfDeletion date   
) INHERITS (Students);
