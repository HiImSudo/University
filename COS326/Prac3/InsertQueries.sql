INSERT INTO Degree Values('BSc','Bachelor of Science', 3, 'IT', 'EBIT');
INSERT INTO Degree Values('BIT','Bachelor of IT', 4, 'IT', 'EBIT');
INSERT INTO Degree Values('PhD','Philosophiac Doctor', 5, 'IT', 'EBIT');

INSERT INTO Courses Values('COS301', 'Software Engineering', 'Computer Science', 40);
INSERT INTO Courses Values('COS326', 'Database Systems', 'Computer Science', 20);
INSERT INTO Courses Values('MTH301', 'Discrete Mathematics', 'Mathematics', 15);
INSERT INTO Courses Values('PHL301', 'Logical Reasoning', 'Philosophy', 15);


INSERT INTO Undergraduates
    Values('140010', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '10-01-1996', 'Bsc', 3, '{"COS301","COS326","MTH301"}');
INSERT INTO Undergraduates
    Values('140015', CAST(ROW('Leja', 'Frank', 'Ms') AS NAME_COMP), '11-05-1995', 'Bsc', 3, '{"COS301","PHL301","MTH301"}');    
INSERT INTO Undergraduates
    Values('131120', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '04-01-1995', 'BIT', 3, '{"COS301","COS326","PHL301"}');
INSERT INTO Undergraduates
    Values('131140', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '05-02-1996', 'BIT', 4, '{"COS301","COS326","MTH301","PHL301"}');

INSERT INTO Postgraduates
    Values('101122', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '05-02-1996', 'PHD', 2, false, CAST(ROW('SuperVisorDude', 'A', 'Mr') AS NAME_COMP));
INSERT INTO Postgraduates
    Values('121101', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '05-02-1996', 'PHD', 3, true, CAST(ROW('SuperVisorDude', 'A', 'Mr') AS NAME_COMP));

