#!/bin/bash

sudo systemctl start postgresql
psql -U postgres -f CreateStatements.sql &> /dev/null
echo "Database created"

psql -U postgres -f InsertQueries.sql &> /dev/null
echo "Populated"

psql -U postgres -f SelectQueries.sql &> /dev/null
psql -U postgres -f SelectQueries.sql &> /dev/null
echo "Added Triggers"

echo "------------ Dictated Inserts ------------"
psql -U postgres -f DictatedInserts.sql


echo "------------ Dictated Selects ------------"
psql -U postgres -f DictatedSelects.sql
