
-- SELECT * FROM Students WHERE isRegisteredFor(Student.CurrentCourses, 'COS326') = True;

-- Should work with Insert 0 1 -- 
-- Valid degree, valid course --
INSERT INTO Undergraduates
    Values('230010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BSc', 3, '{"COS326","MTH301"}');

-- Should work with Insert 0 0 -- 
-- Invalid degree, valid course --
INSERT INTO Undergraduates
    Values('230010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BScc', 3, '{"COS326","MTH301"}');

-- valid degree, invalid course (duplicate) --
INSERT INTO Undergraduates
    Values('230010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BSc', 3, '{"COS326", "COS326","MTH301"}');

-- invalid course code --
INSERT INTO Undergraduates
    Values('230010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BSc', 3, '{"COS33326","MTH301"}');


-- Should log to deletedPostgraduates --

DELETE FROM Postgraduates WHERE StudentNumber = '101122';
SELECT * FROM deletedPostgrad;

-- Should log to deletedUndergraduates --
DELETE FROM Undergraduates WHERE StudentNumber = '140010';
SELECT * FROM deletedUndergrad;