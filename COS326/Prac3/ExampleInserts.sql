-- Should fail with Insert 0 0 --
INSERT INTO Undergraduates
    Values('140010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BScc', 3, '{"COS326","MTH301"}');

-- Should work with Insert 0 1 -- 
INSERT INTO Undergraduates
    Values('230010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BSc', 3, '{"COS326","MTH301"}');


-- Should fail with Insert 0 0 --
INSERT INTO Undergraduates
    Values('140010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BSc', 3, '{"COS326", "COS326","MTH301"}');

-- Should log to deletedPostgrads --
DELETE FROM Postgraduates WHERE StudentNumber = '101122';
SELECT * FROM deletedPostgrad;

-- Should log to deletedUndergraduates --
DELETE FROM Undergraduates WHERE StudentNumber = '230010';
SELECT * FROM deletedUndergrad;
