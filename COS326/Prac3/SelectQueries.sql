DROP FUNCTION personFullNames(NAME_COMP);
CREATE OR REPLACE FUNCTION personFullNames(a NAME_COMP)
RETURNS text AS $$
BEGIN
    RETURN CONCAT(CONCAT(CONCAT(CONCAT(a.Title, ' '),a.Firstname), ' '), a.Surname);
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION ageInYears(date);
CREATE OR REPLACE FUNCTION ageInYears(birthday date)
RETURNS int AS $$
BEGIN
    RETURN extract(year from current_date) - EXTRACT(Year from birthday);
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isFinalYear(int);
CREATE OR REPLACE FUNCTION isFinalYear(b int)
RETURNS boolean AS $$
BEGIN
    RETURN b = 3; 
    /*
        Assuming all courses are 3 years long.
    */
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isFullTime(bool);
CREATE OR REPLACE FUNCTION isFullTime(b boolean)
RETURNS boolean AS $$
BEGIN
    RETURN b; 
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isPartTime(bool);
CREATE OR REPLACE FUNCTION isPartTime(b boolean)
RETURNS boolean AS $$
BEGIN
    RETURN NOT(b); 
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION isRegisteredFor(text[], text);
CREATE OR REPLACE FUNCTION isRegisteredFor(modules text[], module text)
RETURNS boolean AS $$
BEGIN
    RETURN modules @> ARRAY[module];
END
$$ LANGUAGE PLpgSQL;

-- NEW FUNCTIONS --

DROP FUNCTION isValidCourseCode(code_to_check text);
CREATE OR REPLACE FUNCTION isValidCourseCode(code_to_check text)
RETURNS boolean AS $$
BEGIN
    IF EXISTS (select true from Courses where Code=code_to_check) THEN
        RETURN true;
    END IF;
    RETURN false;
END
$$ LANGUAGE PLpgSQL;


-- TODO: CHECK IF THIS IS CORRECT --
DROP FUNCTION hasValidCourseCodes(codes_to_check text[]);
CREATE OR REPLACE FUNCTION hasValidCourseCodes(codes_to_check text[])
RETURNS boolean AS $$
DECLARE c text;
BEGIN
    FOREACH c IN ARRAY codes_to_check
    LOOP
        IF NOT EXISTS (select true from Courses where Code=c) THEN
            RETURN false;
        END IF;
    END LOOP;
    RETURN true;
END
$$ LANGUAGE PLpgSQL;

DROP FUNCTION courseCodeFrequency(code_to_check text);
CREATE OR REPLACE FUNCTION courseCodeFrequency(code_to_check text)
RETURNS int AS $$
DECLARE 
    c text;
    count_var int := 0;
BEGIN
    FOREACH c IN ARRAY codes_to_check
    LOOP
        IF NOT EXISTS (select true from Courses where Code=code_to_check) THEN
            count_var := count_var + 1;
        END IF;
    END LOOP;
    RETURN count_var;
END
$$ LANGUAGE PLpgSQL;


DROP FUNCTION isValidDegreeCode(degree_to_check text);
CREATE OR REPLACE FUNCTION isValidDegreeCode(degree_to_check text)
RETURNS boolean AS $$
BEGIN
    IF EXISTS (SELECT true FROM Degree WHERE DegreeCode=degree_to_check) THEN
        RETURN true;
    END IF;
    RETURN false;
END
$$ LANGUAGE PLpgSQL;


-- Trigger Functions


-- DEGREES START (check_valid_degree)--
CREATE TRIGGER check_valid_degree
BEFORE UPDATE OR INSERT ON Undergraduates FOR EACH ROW
    EXECUTE PROCEDURE check_valid_degree_code();

CREATE TRIGGER check_valid_degree
BEFORE UPDATE OR INSERT ON Students FOR EACH ROW
    EXECUTE PROCEDURE check_valid_degree_code();

CREATE TRIGGER check_valid_degree
BEFORE UPDATE OR INSERT ON Postgraduates FOR EACH ROW
    EXECUTE PROCEDURE check_valid_degree_code();


CREATE OR REPLACE FUNCTION check_valid_degree_code()
RETURNS TRIGGER AS $$
BEGIN 
    IF isValidDegreeCode(NEW.degree)THEN
        RETURN NEW;
    END IF;
    RETURN null;
END
$$ LANGUAGE PLpgSQL;
-- DEGREES END --

-- VALID COURSES START (check_valid_course_registration)--
DROP FUNCTION hasDuplicateCourseCodes(codes_to_check text[]);
CREATE OR REPLACE FUNCTION hasDuplicateCourseCodes(codes_to_check text[])
RETURNS boolean AS $$
DECLARE 
    x text;
    y text;
    count int = 0;
BEGIN
    FOREACH x IN ARRAY codes_to_check
    LOOP
        count = 0;
        FOREACH y IN ARRAY codes_to_check
        LOOP
            IF (x = y) THEN
                count = count + 1;
            END IF;
            IF (count > 1) THEN
                RETURN true;
            END IF;
        END LOOP;
    END LOOP;

    RETURN false;
END
$$ LANGUAGE PLpgSQL;

CREATE TRIGGER check_valid_course_registration
BEFORE UPDATE OR INSERT ON Students FOR EACH ROW
   EXECUTE PROCEDURE check_valid_course_codes();

CREATE TRIGGER check_valid_course_registration
BEFORE UPDATE OR INSERT ON Undergraduates FOR EACH ROW
   EXECUTE PROCEDURE check_valid_course_codes();

CREATE TRIGGER check_valid_course_registration
BEFORE UPDATE OR INSERT ON Postgraduates FOR EACH ROW
   EXECUTE PROCEDURE check_valid_course_codes();

   CREATE OR REPLACE FUNCTION check_valid_course_codes()
RETURNS TRIGGER AS $$
DECLARE x text;
BEGIN 
    IF NOT hasDuplicateCourseCodes(NEW.currentcourses) AND hasValidCourseCodes(NEW.currentcourses) THEN
        RETURN NEW;
    END IF;
    RETURN null;
END
$$ LANGUAGE PLpgSQL;
-- VALID COURSES END --

-- audit_delete_undergrad  START --
CREATE TRIGGER audit_delete_undergrad
BEFORE DELETE ON Undergraduates FOR EACH ROW
    EXECUTE PROCEDURE record_delete_undergraduate();

CREATE OR REPLACE FUNCTION record_delete_undergraduate()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO  DeletedUndergrad VALUES(OLD.studentnumber, OLD.studentname, OLD.birthday, OLD.degree, OLD.yearsofstudy, OLD.currentcourses, user, current_timestamp) ;
    RETURN NEW;
END
$$ LANGUAGE PLpgSQL;
-- audit_delete_undergrad END --

-- audit_delete_postgrad  START --
CREATE TRIGGER audit_delete_postgrad
BEFORE DELETE ON Postgraduates FOR EACH ROW
    EXECUTE PROCEDURE record_delete_postgrad();

CREATE OR REPLACE FUNCTION record_delete_postgrad()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO  DeletedPostgrad VALUES(OLD.studentnumber, OLD.studentname, OLD.birthday, OLD.degree, OLD.yearsofstudy, OLD.isFullTime, OLD.supervisor, user, current_timestamp) ;
    RETURN NEW;
END
$$ LANGUAGE PLpgSQL;
-- audit_delete_postgrad END --


DELETE FROM Postgraduates WHERE StudentNumber = '101122';
SELECT * FROM deletedPostgrad;

-- Should log to deletedUndergraduates --
DELETE FROM Undergraduates WHERE StudentNumber = '140010';
SELECT * FROM deletedUndergrad;