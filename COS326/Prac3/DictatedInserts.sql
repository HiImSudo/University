-- Invalid Degree Code --
INSERT INTO Undergraduates
    Values('140010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BScc', 3, '{"COS326","MTH301"}');

-- Invalid Course Code --
INSERT INTO Undergraduates
    Values('140010', CAST(ROW('Connor', 'Du Plooy Faulty', 'Mr') AS NAME_COMP), '10-01-1996', 'BScc', 3, '{"COS 326","MTH301"}');

-- Invalid Degree Code --

-- INSERT INTO Postgraduates
   -- Values('101122', CAST(ROW('Connor', 'Du Plooy', 'Mr') AS NAME_COMP), '05-02-1996', 'PtestHD', 2, false, CAST(ROW('SuperVisorDude', 'A', 'Mr') AS NAME_COMP));

-- Invalid Degree Code Update --
UPDATE Undergraduates SET Degree = 'COS 326' WHERE StudentNumber = '140010';

-- Invalid Degree Code Update --
UPDATE Undergraduates SET CurrentCourses = '{"COS326", "COS326","MTH301"}' WHERE StudentNumber = '140010';

-- Invalid Degree Code Update --
    -- UPDATE Postgraduates SET Degree = 'COS 326' WHERE StudentNumber = '101122';

-- Should log to deletedPostgrads --
