-- Use unnest to remove trailing {} {} --

DROP FUNCTION clean_from_xmldata(text);
CREATE OR REPLACE FUNCTION clean_from_xmldata(xpath text)
RETURNS xml AS $$
BEGIN
    RETURN unnest(xpath(xpath,CAST(xml_staff.xmldata as XML))) FROM xml_staff;
END
$$ LANGUAGE PLpgSQL;

-- TODO: --
-- Turn the above function into a macro
-- such that unnest(xpath) can be evaluated as a query.
-----------

-- Question 1 --
SELECT clean_from_xmldata('/STAFF[STAFFNO = "SL21"]');

-- Question 2 --
SELECT unnest(xpath('/STAFF[@branchNo = "B003"]/@branchNo',xmldata)) AS branch,
    unnest(xpath('/STAFF[@branchNo = "B003"]/NAME/FNAME',xmldata)) AS fname, 
    unnest(xpath('/STAFF[@branchNo = "B003"]/NAME/LNAME',xmldata)) AS lname
    FROM xml_staff;

-- Question 3 --
SELECT unnest(xpath('/STAFF/STAFFNO/text()',xmldata)) AS Staff,unnest(xpath('/STAFF/SALARY/text()',xmldata)) AS Salary FROM xml_staff;

-- -- Question 4 -- 
SELECT SUM(subquery.total) FROM(SELECT CAST(CAST(unnest(xpath('/STAFF/SALARY/text()',CAST(xmldata AS XML))) AS TEXT) AS INT) AS total FROM xml_staff) AS subquery;

-- -- Question 5 -- 
SELECT AVG(subquery.total) FROM(SELECT CAST(CAST(unnest(xpath('/STAFF/SALARY/text()',CAST(xmldata AS XML))) AS TEXT) AS INT) AS total FROM xml_staff) AS subquery;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

-- -- Question 1 --
SELECT table_to_xmlschema('shred_staff_list',false,false,'')FROM shred_staff_list;

-- -- Question 2 --
SELECT query_to_xml('SELECT * FROM shred_staff_list WHERE CAST(salary AS INTEGER) = 30000;',false,false,'') FROM shred_staff_list;

-- -- Question 3 --
SELECT row_to_json(ali) FROM (SELECT * FROM shred_staff_list WHERE salary = '30000') ali;
