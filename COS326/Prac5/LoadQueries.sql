/*
    Table Names:
        xml_staff_list
        xml_staff
        shred_staff_list
*/

DROP DATABASE cos326;
CREATE DATABASE cos326;

-- Question 1 --
DROP TABLE xml_staff_list CASCADE;
CREATE TABLE xml_staff_list (
    fname text,
    xmldoc xml
);

DROP TABLE xml_staff CASCADE;
CREATE TABLE xml_staff (
    entry_number SERIAL,
    xmldata xml
);

DROP TABLE shred_staff_list CASCADE;
CREATE TABLE shred_staff_list (
    branchNo text,
    staffNo text,
    fname text,
    lname text,
    position text,
    DOB text,
    salary text
);

-- ***** NB: PLEASE USE THE ./go.sh SCRIPT ****** --

-- Macro for working with the xml document --

DROP FUNCTION xdata();
CREATE OR REPLACE FUNCTION xdata()
RETURNS xml AS $$
BEGIN
    RETURN XMLPARSE(DOCUMENT convert_from(pg_read_binary_file('staff_list.xml'), 'UTF8'));
END
$$ LANGUAGE PLpgSQL;

-- Question 2 --
INSERT INTO xml_staff_list VALUES('staff_list.xml', XMLPARSE(DOCUMENT convert_from(
        pg_read_binary_file('staff_list.xml'), 'UTF8')));


-- Question 3 --
DROP FUNCTION loadXMLFragments(text, xml);
CREATE OR REPLACE FUNCTION loadXMLFragments(xpath text,vals xml)
RETURNS boolean AS $$
DECLARE
    dataArr xml[];
    x xml;
BEGIN
    dataArr := xpath(xpath, vals);
    FOREACH x IN ARRAY dataArr
    LOOP
        INSERT INTO xml_staff VALUES (DEFAULT, x);
    END LOOP;
    RETURN xmlexists(xpath PASSING BY REF vals);
END
$$ LANGUAGE PLpgSQL;


-- An example of selecting the entire xml doc --

-- Question 4 --
-- TODO: Finish Inserting Into xml_staff_table
SELECT loadXMLFragments('//STAFFLIST/STAFF', xdata());
SELECT * FROM xml_staff;

-- Question 5 --
DROP FUNCTION loadshreddedXML(text, xml);
CREATE OR REPLACE FUNCTION loadshreddedXML (xpath text,vals xml) RETURNS BOOLEAN AS
$$
	DECLARE
        i int;
		DOB text[];
		staffNo text[];
		fname text[];
		pos text[];
		lname text[];
		salary text[];		
		branchNo text[];
	BEGIN
		branchNo :=  xpath(CONCAT(xpath,'/@branchNo'),vals);
		staffNo :=  xpath(CONCAT(xpath,'/STAFFNO/text()'),vals);
		fname :=  xpath(CONCAT(xpath,'/NAME/FNAME/text()'),vals);
		lname :=  xpath(CONCAT(xpath,'/NAME/LNAME/text()'),vals);
		pos :=  xpath(CONCAT(xpath, '/POSITION/text()'),vals);
		DOB :=  xpath(CONCAT(xpath, '/DOB/text()'),vals);
		salary :=  xpath(CONCAT(xpath, '/SALARY/text()'),vals);
		
        i := 0;
        LOOP
        EXIT WHEN  i = array_length(salary, 1);
            i := i + 1;
            INSERT INTO shred_staff_list VALUES(branchNo[i],staffNo[i],fname[i],lname[i],pos[i],DOB[i],salary[i]);
        END LOOP;

		RETURN true;
	END;
$$LANGUAGE plpgSQL;

-- Question 6 -- 
SELECT loadshreddedXML('//STAFFLIST/STAFF', xdata());
SELECT * FROM shred_staff_list WHERE branchNo = 'B001';
